<?php
	if(@$_POST['submit']){
		$kode_jadwal = $_POST['kode_jadwal'];
		$kode_kelas = $_POST['kelas'];
		$kode_dosen = $_POST['nama_dosen'];
		$kode_ruangan = $_POST['ruangan'];
		$mata_kuliah = $_POST['mata_kuliah'];
		$sks = $_POST['sks'];
		$tanggal = $_POST['thn']."-".$_POST['bln']."-".$_POST['tgl'];
		
		mysqli_query($cn,"UPDATE jadwal SET kode_kelas='".$kode_kelas."',
		kode_dosen='".$kode_dosen."',kode_ruangan='".$kode_ruangan."',
		mata_kuliah='".$mata_kuliah."',sks=".$sks.",
		tanggal='".$tanggal."' WHERE kode_jadwal='".$kode_jadwal."'");
		
		header('Location:index.php?page=list_jadwal');
		exit();
	}
	
	$sw_kode_jadwal = @$_GET['kode'];
	$qr_jadwal = mysqli_query($cn,"SELECT * FROM jadwal as jd 
			INNER JOIN kelas as kl ON jd.kode_kelas=kl.kode_kelas 
			INNER JOIN ruangan as rk ON jd.kode_ruangan=rk.kode
			INNER JOIN dosen as ds ON jd.kode_dosen=ds.kode_dosen 
			WHERE kode_jadwal='".$sw_kode_jadwal."'");
	 while($f_jadwal = mysqli_fetch_array($qr_jadwal)){
		$sw_kode_kelas = $f_jadwal['kode_kelas'];
		$sw_kode_ruangan = $f_jadwal['kode'];
		$sw_kode_dosen = $f_jadwal['kode_dosen'];
		$sw_mata_kuliah = $f_jadwal['mata_kuliah'];
		$sw_sks = $f_jadwal['sks'];
		$sw_tgl = explode("-",$f_jadwal['tanggal']);
	}
?>
            <form class="mt-2" method="post" action="index.php?page=edit_jadwal">
            <h4 class="mb-3">Edit Data Jadwal</h4>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Kode Jadwal</label>
                <div class="col-sm-3">
                <input type="text" class="form-control" placeholder="Kode Jadwal" name="kode_jadwal" value="<?=$sw_kode_jadwal;?>" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Kelas</label>
                <div class="col-sm-3">
                <select class="form-control" name="kelas">
                	<option value="0">-- Pilih kelas --</option>
					<?php
                    $qr_kelas = mysqli_query($cn, 'SELECT * FROM kelas');
					while($f_kelas = mysqli_fetch_array($qr_kelas)){
						if($sw_kode_kelas == $f_kelas['kode_kelas']){
							$slc_kls = 'selected';
						}else{
							$slc_kls = '';
						}
					?>
                	<option <?=$slc_kls;?> value="<?=$f_kelas['kode_kelas']?>">
                    	<?=$f_kelas['kelas'];?>
                    </option>
                	<?php
					}
					?>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Tanggal</label>
                <div class="col-sm-3">
                <select class="form-control" name="tgl">
                	<option value="0">-- Pilih Tanggal --</option>
					<?php
                    for($tgl=1;$tgl<=31;$tgl++){
						if($sw_tgl[2] == $tgl){
							$slc_tgl = 'selected';
						}else{
							$slc_tgl = '';
						}
					?>
                    <option <?=$slc_tgl;?> value="<?=$tgl;?>">
						<?=$tgl;?>
                    </option>
					<?php
					}
					?>
                </select>
                </div>
                <div class="col-sm-3">
                <select class="form-control" name="bln">
                	<option value="0">-- Pilih Bulan --</option>
                    <?php
                    $txt_bln = array('Januari','Februari','Maret','April',
								'Mei','Juni','Juli','Agustus','September',
								'Oktober','November','Desember');
					for($bln=0;$bln<12;$bln++){
						if($sw_tgl[1] == $bln+1){
							$slc_bln = 'selected';
						}else{
							$slc_bln = '';
						}
					?>
                    <option <?=$slc_bln;?> value="<?=$bln+1;?>">
                    	<?=$txt_bln[$bln];?>
                    </option>
					<?php
					}
					?>
                </select>
                </div>
                <div class="col-sm">
				<input type="text" class="form-control" placeholder="Tahun" name="thn" value="<?=$sw_tgl[0];?>">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nama Dosen</label>
                <div class="col-sm-3">
                <select class="form-control" name="nama_dosen">
                	<option value="0">-- Pilih Dosen --</option>
                    <?php
                    $qr_dosen = mysqli_query($cn,'SELECT * FROM dosen');
					while($f_dosen = mysqli_fetch_array($qr_dosen)){
						if($sw_kode_dosen == $f_dosen['kode_dosen']){
							$slc_dosen = 'selected';
						}else{
							$slc_dosen = '';
						}
					?>
                	<option <?=$slc_dosen;?> value="<?=$f_dosen['kode_dosen'];?>">
                    	<?=$f_dosen['nama'];?>
                    </option>
                    <?php
					}
					?>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Ruangan</label>
                <div class="col-sm-3">
                <select class="form-control" name="ruangan">
                	<option value="0">-- Pilih Ruangan --</option>
                    <?php
                    $qr_ruangan=mysqli_query($cn,'SELECT * FROM ruangan');
					while($f_ruangan=mysqli_fetch_array($qr_ruangan)){
						if($sw_kode_ruangan == $f_ruangan['kode']){
							$slc_ruangan = 'selected';
						}else{
							$slc_ruangan = '';
						}
					?>
                    <option <?=$slc_ruangan;?> value="<?=$f_ruangan['kode_ruangan'];?>">
                    	<?=$f_ruangan['nama_ruangan'];?>
                    </option>
                    <?php
					}
					?>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Matakuliah</label>
                <div class="col-sm-3">
                <input type="text" class="form-control" placeholder="Matakuliah" name="mata_kuliah" value="<?=$sw_mata_kuliah;?>">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">SKS</label>
                <div class="col-sm-3">
                <input type="text" class="form-control" placeholder="SKS" name="sks" value="<?=$sw_sks;?>">
                </div>
              </div>
              <input name="submit" class="btn btn-primary" type="submit" value="Submit">
			  <input class="btn btn-secondary" type="reset" value="Reset">
            </form>