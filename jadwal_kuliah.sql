-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 28, 2019 at 08:02 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jadwal_kuliah`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_log`
--

CREATE TABLE `admin_log` (
  `id_log` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `sesi` varchar(50) NOT NULL,
  `last_login` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_log`
--

INSERT INTO `admin_log` (`id_log`, `id_user`, `sesi`, `last_login`) VALUES
(16, 1, '-', '2018-05-25 15:03:06'),
(17, 2, '-', '2018-05-25 14:56:46');

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

CREATE TABLE `dosen` (
  `kode_dosen` varchar(5) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `rate` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`kode_dosen`, `nama`, `tanggal_lahir`, `alamat`, `telp`, `rate`) VALUES
('DS001', 'Abdul Hamkah', '1985-04-22', 'Jl. Plumpang Raya No. 66', '085711224422', 25000),
('DS002', 'Iqbal Yahya', '1988-03-11', 'Jl. Medan Barat No. 70', '085244556688', 230000),
('DS003', 'Ali Imran', '1990-01-20', 'Jl. Majapahit No. 22', '081313221515', 200000),
('DS004', 'Sunarto', '1983-12-28', 'Jl. Melati Timur No. 99', '088811335533', 220000),
('DS005', 'Alfin Faris', '1991-09-05', 'Jl. Gajah Mada No. 42', '081322667766', 250000);

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `kode_jadwal` varchar(10) NOT NULL,
  `kode_kelas` varchar(10) NOT NULL,
  `kode_dosen` varchar(5) NOT NULL,
  `kode_ruangan` varchar(5) NOT NULL,
  `mata_kuliah` varchar(50) NOT NULL,
  `sks` int(11) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`kode_jadwal`, `kode_kelas`, `kode_dosen`, `kode_ruangan`, `mata_kuliah`, `sks`, `tanggal`) VALUES
('JD201901', 'KL002', 'DS003', 'RK004', 'Dasar dasar pemograman', 4, '2019-08-27'),
('JD201902', 'KL004', 'DS001', 'RK005', 'Dasar dasar akuntansi', 2, '2019-08-27'),
('JD201903', 'KL001', 'DS004', 'RK001', 'Kewirausahaan', 2, '2019-08-27'),
('JD201904', 'KL003', 'DS005', 'RK002', 'Pemograman web 1', 4, '2019-08-27'),
('JD201905', 'KL005', 'DS003', 'RK003', 'Perpajakan 1', 4, '2019-08-27');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `kode_kelas` varchar(10) NOT NULL,
  `kelas` varchar(10) NOT NULL,
  `jurusan` varchar(30) NOT NULL,
  `angkatan` int(11) NOT NULL,
  `tingkat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`kode_kelas`, `kelas`, `jurusan`, `angkatan`, `tingkat`) VALUES
('KL001', 'AP-13-020', 'Administrasi Bisnis', 2016, 2),
('KL002', 'IK-13-001', 'Management Informatika', 2016, 2),
('KL003', 'IK-13-002', 'Management Informatika', 2015, 3),
('KL004', 'KA-13-010', 'Komputer Akuntansi', 2016, 2),
('KL005', 'KA-13-011', 'Komputer Akuntansi', 2015, 3);

-- --------------------------------------------------------

--
-- Table structure for table `ruangan`
--

CREATE TABLE `ruangan` (
  `kode_ruangan` varchar(5) NOT NULL,
  `nama_ruangan` varchar(25) NOT NULL,
  `letak` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruangan`
--

INSERT INTO `ruangan` (`kode_ruangan`, `nama_ruangan`, `letak`) VALUES
('RK001', 'Galaxy', 'Lantai 3'),
('RK002', 'Jelly Bean', 'Lantai 1'),
('RK003', 'Google', 'Lantai 1'),
('RK004', 'Eclair', 'Lantai 2'),
('RK005', 'Oreo', 'Lantai 3');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `level`) VALUES
(1, 'admin', '0192023a7bbd73250516f069df18b500', 1),
(2, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_log`
--
ALTER TABLE `admin_log`
  ADD PRIMARY KEY (`id_log`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`kode_dosen`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`kode_jadwal`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`kode_kelas`);

--
-- Indexes for table `ruangan`
--
ALTER TABLE `ruangan`
  ADD PRIMARY KEY (`kode_ruangan`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_log`
--
ALTER TABLE `admin_log`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_log`
--
ALTER TABLE `admin_log`
  ADD CONSTRAINT `admin_log_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
