<head>
<title>Aplikasi Pengolahan Jadwal Kuliah</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>

<body class="container-fluid">
    <nav class="navbar navbar-dark bg-primary">
        <a class="navbar-brand" href="index.php">Aplikasi Pengelolaan Jadwal Kuliah [Nama Anda masing-masing]</a>
    </nav>
	<div class="row">
    	<div class="col-2">
            <ul class="nav flex-column border">
              <li class="nav-item bg-dark">
                <a class="nav-link text-center text-white disabled" href="#">MENU</a>
              </li>
              <li class="nav-item border">
                <a class="nav-link disabled" href="#">Form</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Data Dosen</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Data Kelas</a>
              </li>
			  <li class="nav-item">
                <a class="nav-link" href="#">Data Ruangan</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="index.php?page=input_jadwal">
                	Jadwal
				</a>
              </li>
              <li class="nav-item border">
                <a class="nav-link disabled" href="#">Laporan</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">List Dosen</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">List Kelas</a>
              </li>
			  <li class="nav-item">
                <a class="nav-link" href="index.php?page=list_jadwal">
                	List Data Berobat
                </a>
              </li>
            </ul>
        </div>
        <div class="col mr-3 border bg-light">
        	<?php
				include 'koneksi.php';
            	$page = @$_GET['page'];
				if($page == 'list_jadwal'){
					include 'list_jadwal.php';
				}elseif($page == 'input_jadwal'){
					include 'input_jadwal.php';
				}elseif($page == 'edit_jadwal'){
					include 'edit_jadwal.php';
				}
			?>
        </div>
    </div>
</body>
</html>